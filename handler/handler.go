package handler

// Callback callback function when message received
type Callback func(data []byte, ack Ack) error

// Message callback return message
type Message struct {
	data []byte
	ack  Ack
}

// Ack callback return ack handler
type Ack func() error

// ListenerValue listener return value
type ListenerValue struct {
	Unsubscribe func() error
}
