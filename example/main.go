package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/sehat-oss/kopaja"
	"gitlab.com/sehat-oss/kopaja/config"
	"gitlab.com/sehat-oss/kopaja/handler"
)

// Hello define hello struct
type Hello struct {
	Msg string
}

func main() {
	client, err := kopaja.Connect(config.Config{
		ClusterID: "cherry_ft",
		ClientID:  "cherry_ft_pub",
	})
	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	topic := "hello-topic"
	durable := fmt.Sprintf("%s-durable", topic)

	hello := &Hello{
		Msg: "hello from test topic \n",
	}

	byteMessage, err := json.Marshal(hello)
	if err != nil {
		log.Fatalf("Error on marshaling strMsg: %s", err)
	}

	client.Emit(topic, byteMessage)

	sub, err := client.Listen(topic, durable, func(data []byte, ack handler.Ack) (err error) {
		ack()

		res := Hello{}
		err = json.Unmarshal(data, &res)
		if err != nil {
			log.Printf("Error unmarshal data: %s", err)
			return
		}

		fmt.Printf("Data: %s", res.Msg)

		return
	})

	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			fmt.Printf("\nReceived an interrupt, unsubscribing and closing connection...\n\n")
			// Do not unsubscribe a durable on exit, except if asked to.
			if durable == "" {
				sub.Unsubscribe()
			}
			client.Close()
			cleanupDone <- true
		}
	}()
	<-cleanupDone
}
