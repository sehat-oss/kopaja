package main

import (
	"encoding/json"
	"log"

	"gitlab.com/sehat-oss/kopaja"
	"gitlab.com/sehat-oss/kopaja/config"
)

type stringMessage struct {
	Msg string
}

func main() {
	client, err := kopaja.Connect(config.Config{
		ClusterID: "cherry_ft",
		ClientID:  "cherry_ft_pub",
	})
	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	strMsg := &stringMessage{
		Msg: "hello from test topic \n",
	}

	byteMessage, err := json.Marshal(strMsg)
	if err != nil {
		log.Fatalf("Error on marshaling strMsg: %s", err)
	}

	client.Emit("test-topic", byteMessage)
}
