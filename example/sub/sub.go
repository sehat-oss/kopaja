package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/sehat-oss/kopaja/handler"

	"gitlab.com/sehat-oss/kopaja"
	"gitlab.com/sehat-oss/kopaja/config"
)

func main() {
	client, err := kopaja.Connect(config.Config{
		ClusterID: "cherry_ft",
		ClientID:  "cherry_ft",
	})
	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	topic := "test-topic"
	durable := "" //fmt.Sprintf("%s-durable", topic)

	sub, err := client.Listen(topic, durable, callback)
	if err != nil {
		log.Printf("Error listener: %s", err)
	}

	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			fmt.Printf("\nReceived an interrupt, unsubscribing and closing connection...\n\n")

			// Do not unsubscribe a durable on exit.
			if durable == "" {
				sub.Unsubscribe()
			}

			client.Close()
			cleanupDone <- true
		}
	}()
	<-cleanupDone
}

type stringMessage struct {
	Msg string
}

func callback(data []byte, ack handler.Ack) (err error) {
	ack()

	natsMsg := stringMessage{}
	err = json.Unmarshal(data, &natsMsg)
	if err != nil {
		log.Printf("Error unmarshal data: %s", err)
		return
	}

	fmt.Printf("Data: %s", natsMsg.Msg)
	return
}
