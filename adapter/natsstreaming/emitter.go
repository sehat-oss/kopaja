package natsstreaming

// Emit emit/publish to any topic on nats streaming cluster
func (c *Client) Emit(topic string, payload []byte) (err error) {
	err = c.conn.Publish(topic, payload)
	return
}
