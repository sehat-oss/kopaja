package natsstreaming

import (
	"fmt"
	"log"

	kopajaerror "gitlab.com/sehat-oss/kopaja/error"

	stan "github.com/nats-io/stan.go"
	"gitlab.com/sehat-oss/kopaja/config"
)

// Client define client implementation for nats streaming
type Client struct {
	conn stan.Conn
}

// SetConfig set nats streaming configuration
func (c *Client) SetConfig(cfg config.Config) (err error) {

	if &cfg.ClusterID == nil {
		return kopajaerror.ErrNoClusterIDProvided
	}

	if &cfg.ClientID == nil {
		return kopajaerror.ErrNoClientIDProvided
	}

	url := c.getURL(cfg.Host, cfg.Port)
	log.Printf("host: %s", url)

	c.conn, err = stan.Connect(
		cfg.ClusterID,
		cfg.ClientID,
		stan.NatsURL(url),
	)

	return
}

func (c *Client) getURL(host string, port uint64) (url string) {
	if host == "" && port == 0 {
		url = stan.DefaultNatsURL
		return
	}

	if host == "" {
		url = fmt.Sprintf("nats://localhost:%d", port)
	}

	if port == 0 {
		url = fmt.Sprintf("nats://%s:4222", host)
	}

	return
}

//Close close nats streaming connection
func (c *Client) Close() (err error) {
	err = c.conn.Close()
	return
}
