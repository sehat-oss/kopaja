package natsstreaming

import (
	"time"

	stan "github.com/nats-io/stan.go"
	"gitlab.com/sehat-oss/kopaja/handler"
)

// Listen listen/publish to any topic on nats streaming cluster
func (c *Client) Listen(topic string, durable string, handler handler.Callback) (listenerValue handler.ListenerValue, err error) {
	aw, err := time.ParseDuration("60s")
	if err != nil {
		return
	}

	sub, err := c.conn.Subscribe(topic, func(msg *stan.Msg) {
		handler(msg.Data, msg.Ack)
	},
		stan.DurableName(durable),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
	)
	if err != nil {
		return
	}

	listenerValue.Unsubscribe = sub.Unsubscribe

	return
}
