package kopajaerror

import "errors"

var (
	ErrNoExchangeProvided  = errors.New("Exchange is required")
	ErrNoChannelProvided   = errors.New("Channel is required")
	ErrNoClusterIDProvided = errors.New("ClusterID is required")
	ErrNoClientIDProvided  = errors.New("ClientID is required")
)
