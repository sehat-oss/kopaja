package config

type Config struct {
	Type      string
	Host      string
	Port      uint64
	Username  string
	Password  string
	Exchange  string
	Channel   string
	ClusterID string
	ClientID  string
}
