# kopaja

**Kopaja** is a public transportation in Jakarta that transports people from one place to another. On sehat environment, **kopaja** will be transports any massage from any services to another.   
The main goal of **kopaja** is to be an eventbus wrapper for any message broker to avoid tight coupling between services and message broker.

## support
currently kopaja is support for nats streaming only. In the near future will be support for nsq, apache kafka, rabbitmq and activemq.   

## how to install
get kopaja using the command below   
```bash
go get gitlab.com/sehat-oss/kopaja
```

## how to use
below is the example how to use kopaja    
```go
// Hello define hello struct
type Hello struct {
	Msg string
}

func main() {
	client, err := kopaja.Connect(config.Config{
		ClusterID: "cherry_ft",
		ClientID:  "cherry_ft_pub",
	})
	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	topic := "hello-topic"
	durable := fmt.Sprintf("%s-durable", topic)

	hello := &Hello{
		Msg: "hello from test topic \n",
	}

	byteMessage, err := json.Marshal(hello)
	if err != nil {
		log.Fatalf("Error on marshaling strMsg: %s", err)
	}

	client.Emit(topic, byteMessage)

	sub, err := client.Listen(topic, durable, func(data []byte, ack handler.Ack) (err error) {
		ack()

		res := Hello{}
		err = json.Unmarshal(data, &res)
		if err != nil {
			log.Printf("Error unmarshal data: %s", err)
			return
		}

		fmt.Printf("Data: %s", res.Msg)

		return
	})

	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			fmt.Printf("\nReceived an interrupt, unsubscribing and closing connection...\n\n")
			// Do not unsubscribe a durable on exit, except if asked to.
			if durable == "" {
				sub.Unsubscribe()
			}
			client.Close()
			cleanupDone <- true
		}
	}()
	<-cleanupDone
}
```