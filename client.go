package kopaja

import (
	"gitlab.com/sehat-oss/kopaja/adapter/natsstreaming"
	"gitlab.com/sehat-oss/kopaja/config"
	"gitlab.com/sehat-oss/kopaja/handler"
)

// Client define kopaja client interface
type Client interface {
	SetConfig(cfg config.Config) (err error)
	Emit(topic string, payload []byte) (err error)
	Listen(topic string, durable string, handler handler.Callback) (sub handler.ListenerValue, err error)
	Close() (err error)
}

// Connect connect to message broker and return kopaja client
func Connect(cfg config.Config) (client Client, err error) {
	client = new(natsstreaming.Client)
	err = client.SetConfig(cfg)
	return
}
